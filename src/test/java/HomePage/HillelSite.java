package HomePage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static pages.HomePageElements.*;
//import static org.testng.Assert.*;

public class HillelSite {

    WebDriver driver;
    String site = "https://ithillel.ua/";

    @BeforeClass
    public void beforeSetting1() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver100.exe");
    }


    @BeforeMethod
    public void beforeSetting2() {
        driver = new ChromeDriver();
        driver.get(site);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }


    @AfterMethod
    public void afterSetting() {
        driver.quit();
    }


    @Test(enabled = true)//"does NOT find //button[@class='btn -small -light cookie-ntf_agreement'] element"
    public void acceptCookiesCloseInteresst() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CLOSE_COOKIES_POPUP)));
//        assertTrue(driver.findElement(By.xpath(CLOSE_COOKIES_POPUP)).isDisplayed());
        assertThat((driver.findElement(By.xpath(CLOSE_COOKIES_POPUP))), notNullValue());
        driver.findElement(By.xpath(CLOSE_COOKIES_POPUP)).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(COOKIES_CLOSE_BUTTON)));
        driver.findElement(By.xpath(COOKIES_CLOSE_BUTTON)).click();

    }


    @Test
    public void logoRedirectsToHomepage1() {
        driver.findElement(By.xpath(KHARKOV)).click();

//        assertTrue(driver.getCurrentUrl().equalsIgnoreCase("https://kharkiv.ithillel.ua/"));
//        driver.quit();

        assertThat(driver.getCurrentUrl(), is(equalTo("https://kharkiv.ithillel.ua/ua/")));
    }

//    @Test
//    public void checklabel(){
//        driver.findElement(By.xpath())
//        assertThat(driver.getTitle(), );
//    }


    @Test(dataProvider = "test-data")
    public void subscribeNews(String data) {
        WebDriverWait wait = new WebDriverWait(driver, 30);

        driver.findElement(By.xpath(EMAIL_FIELD)).sendKeys(data);


        driver.findElement(By.xpath("//*[@id='form-mailing']//button")).click();
        wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath(COOKIES_CLOSE_BUTTON), ""));
//        assertEquals(driver.findElement(By.xpath(EMAIL_FIELD)).getText(), "");
//        driver.quit();
        assertThat(driver.findElement(By.xpath(EMAIL_FIELD)).getText(), is(equalToIgnoringCase("")));
        driver.quit();
    }


    @Ignore("in progress, not implemented yet")
    public void bestStudentAlexM() {
        assertThat(driver.getCurrentUrl(), equalToIgnoringCase("//@h2='Александр Мисевра - Лучший студент курса за 5 лет'"));
    }


    @DataProvider(name = "test-data")
    public Object[][] dataProvFunc() {
        return new Object[][]{{"Lambda Test"}, {"Automation"}};
    }
}