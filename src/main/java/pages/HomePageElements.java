package pages;

import org.openqa.selenium.By;

public class HomePageElements {

    public static final String KHARKOV = "//*[@data-city-id='kh']";
    public static final String COOKIES_CLOSE_BUTTON = "//*[@id=\"form-lead-magnet\"]/div/button";
    public static final String EMAIL_FIELD = "//input[@id='input-email-mailing']";
    public static final String CLOSE_COOKIES_POPUP = "//button[@class='btn -small -light cookie-ntf_agreement']";

    public static final By CLOSE_COOKIES_POPUP_BY = By.xpath("//button[@class='btn -small -light cookie-ntf_agreement']");


}
